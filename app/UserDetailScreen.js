import React, { Component } from 'react';
import {
    Alert,
    Button,
    StyleSheet,
    ScrollView,
    ActivityIndicator,
    View,
    TouchableOpacity,Text
} from 'react-native';
import { TextInput } from 'react-native-paper';
import firebase from '../app/firebaseDb';

const db = firebase.firestore();
db.settings({ experimentalForceLongPolling: true });

class UserDetailScreen extends Component {
    constructor() {
        super();
        this.state = {
            name: '',
            email: '',
            mobile: '',
            isLoading: true,
        };
    }

    async componentDidMount() {
        await db
            .collection('users')
            .doc(this.props.route.params.userkey)
            .get()
            .then((res) => {
                if (res.exists) {
                    const user = res.data();
                    this.setState({
                        key: res.id,
                        name: user.name,
                        email: user.email,
                        mobile: user.mobile,
                        isLoading: false,
                    });
                } else {
                    console.log('Document does not exist!');
                }
            });
    }

    inputValueUpdate = (val, prop) => {
        const state = this.state;
        state[prop] = val;
        this.setState(state);
    };

    updateUser() {
        this.setState({
            isLoading: true,
        });
        const updateDBRef = firebase
            .firestore()
            .collection('users')
            .doc(this.state.key);
        updateDBRef
            .set({
                name: this.state.name,
                email: this.state.email,
                mobile: this.state.mobile,
            })
            .then((docRef) => {
                this.setState({
                    key: '',
                    name: '',
                    email: '',
                    mobile: '',
                    isLoading: false,
                });
                this.props.navigation.navigate('AddUserScreen');
            })
            .catch((error) => {
                console.error('Error: ', error);
                this.setState({
                    isLoading: false,
                });
            });
    }

    deleteUser() {
        const dbRef = firebase
            .firestore()
            .collection('users')
            .doc(this.props.route.params.userkey);
        dbRef.delete().then((res) => {
            console.log('Item removed from database');
            this.props.navigation.navigate('AddUserScreen');
        });
    }

    openTwoButtonAlert = () => {
        Alert.alert(
            'Eliminar Usuario',
            'Esta seguro que desea eliminar este usuario?',
            [
                { text: 'Si', onPress: () => this.deleteUser() },
                {
                    text: 'No',
                    onPress: () => console.log('No item was removed'),
                    style: 'cancel',
                },
            ],
            {
                cancelable: true,
            },
        );
    };

    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.preloader}>
                    <ActivityIndicator size="large" color="#9E9E9E" />
                </View>
            );
        }
        return (
            <ScrollView style={styles.container}>
                <View style={styles.inputGroup}>
                    <TextInput
                        mode={'outlined'}
                        label={'Name'}
                        value={this.state.name}
                        onChangeText={(val) => this.inputValueUpdate(val, 'name')}
                    />
                </View>
                <View style={styles.inputGroup}>
                    <TextInput
                        mode={'outlined'}
                        label={'Email'}
                        value={this.state.email}
                        onChangeText={(val) => this.inputValueUpdate(val, 'email')}
                    />
                </View>
                <View style={styles.inputGroup}>
                    <TextInput
                        mode={'outlined'}
                        label={'Mobil1e'}
                        value={this.state.mobile}
                        onChangeText={(val) => this.inputValueUpdate(val, 'mobile')}
                    />
                </View>
                <View>
                <TouchableOpacity
                    onPress={() => this.updateUser()}
                    style={{
                        backgroundColor: 'black',
                        borderRadius: 10,
                        padding: 10,
                        marginBottom: 20,
                    }}>
                    <Text style={{ color: 'white', alignSelf: 'center' }}>
                        Editar
            </Text>
                </TouchableOpacity>
                </View>
                <View>
                <TouchableOpacity
                    onPress={this.openTwoButtonAlert}
                    style={{
                        backgroundColor: 'red',
                        borderRadius: 10,
                        padding: 10,
                        marginBottom: 20,
                    }}>
                    <Text style={{ color: 'white', alignSelf: 'center' }}>
                        Eliminar
            </Text>
                </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 35,
    },
    inputGroup: {
        flex: 1,
        padding: 0,
        marginBottom: 15,
        borderBottomWidth: 1,
        borderBottomColor: '#cccccc',
    },
    preloader: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

export default UserDetailScreen;
