
import * as firebase from 'firebase';
import firestore from 'firebase/firestore'


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyB_HnihhE1_RdHhHZDI3BB188JFtIzPxPA",
    authDomain: "project009-f81b3.firebaseapp.com",
    databaseURL: "https://project009-f81b3.firebaseio.com",
    projectId: "project009-f81b3",
    storageBucket: "project009-f81b3.appspot.com",
    messagingSenderId: "245561451730",
    appId: "1:245561451730:web:7b5301f064d5aad2a8f90b",
};

firebase.initializeApp(firebaseConfig);

export default firebase;
